from rest_framework import serializers
from faq.models import Question,Answer,ProblemType


class QuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model= Question
        fields= ['id','title','problemID','description']

class AnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model= Answer
        fields= ['id','answer','idquestion']

class ProblemTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model= ProblemType
        fields= ['id','description']

