
from django.shortcuts import render
from rest_framework import generics
from faq.models import Answer,Question,ProblemType
from faq.serializers import AnswerSerializer,QuestionSerializer,ProblemTypeSerializer

# Create your views here.

class AnswerList(generics.ListCreateAPIView):
    queryset= Answer.objects.all()
    serializer_class= AnswerSerializer

class AnswerDetail(generics.RetrieveUpdateAPIView):
    queryset= Answer
    serializer_class= AnswerSerializer

class QuestionList(generics.ListCreateAPIView):
    queryset= Question.objects.all()
    serializer_class= QuestionSerializer

class QuestionDetail(generics.RetrieveUpdateAPIView):
    queryset= Question
    serializer_class= QuestionSerializer    


class QuestionList(generics.ListCreateAPIView):
    queryset= Question.objects.all()
    serializer_class= QuestionSerializer

class QuestionDetail(generics.RetrieveUpdateAPIView):
    queryset= Question
    serializer_class= QuestionSerializer


class ProblemTypeList(generics.ListCreateAPIView):
    queryset= ProblemType.objects.all()
    serializer_class= ProblemTypeSerializer

class ProblemTypeDetail(generics.RetrieveUpdateAPIView):
    queryset= ProblemType
    serializer_class= ProblemTypeSerializer
