"""faqapi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from faq.views import QuestionList,QuestionDetail,AnswerList,AnswerDetail,ProblemTypeDetail,ProblemTypeList


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/questions',QuestionList.as_view()),
    path('api/questions/<int:pk>',QuestionDetail.as_view()),
    path('api/answers',AnswerList.as_view()),
    path('api/answers/<int:pk>',AnswerDetail.as_view()),
    path('api/problem-type',ProblemTypeList.as_view()),
    path('api/problem-type/<int:pk>',ProblemTypeDetail.as_view()),
]
